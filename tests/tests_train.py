import importlib
import os

import styleclass.train

from styleclass.features import read_dataset

from unittest import TestCase
from unittest.mock import patch


class TrainingTestCase(TestCase):
    def test_train(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        count_vectorizer, _, model = styleclass.train.train(dataset,
                                                            random_state=0)

        self.assertEqual(len(count_vectorizer.get_feature_names()), 510)
        self.assertEqual(model.classes_.shape, (3, ))
        self.assertEqual(model.coef_.shape, (3, 511))
        self.assertEqual(model.intercept_.shape, (3, ))
        self.assertEqual(set(model.classes_),
                         set(['apa', 'bmc-bioinformatics', 'ieee']))

    @patch('styleclass.settings.N_FEATURES', 250)
    def test_train_with_features(self):
        importlib.reload(styleclass.train)
        dataset = styleclass.features.read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        count_vectorizer, _, model = styleclass.train.train(dataset,
                                                            random_state=0)

        self.assertEqual(len(count_vectorizer.get_feature_names()), 250)
        self.assertEqual(model.classes_.shape, (3, ))
        self.assertEqual(model.coef_.shape, (3, 251))
        self.assertEqual(model.intercept_.shape, (3, ))
        self.assertEqual(set(model.classes_),
                         set(['apa', 'bmc-bioinformatics', 'ieee']))
