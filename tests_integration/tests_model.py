import pandas as pd

from unittest import TestCase
from styleclass.features import clean_dataset, add_noise, generate_unknown, \
    get_default_training_dataset, get_default_test_dataset
from styleclass.settings import N_FEATURES, STYLES
from styleclass.train import train, get_default_algorithm, get_default_model
from styleclass.classify import evaluate_model, evaluate_cv
from statistics import mean


class TrainingTestCase(TestCase):
    def test_train(self):
        dataset = get_default_training_dataset()
        dataset = clean_dataset(dataset)
        dataset = add_noise(dataset, random_state=0)
        count_vectorizer, _, model = train(dataset, random_state=0)

        self.assertEqual(len(count_vectorizer.get_feature_names()), N_FEATURES)
        self.assertEqual(model.classes_.shape, (len(STYLES), ))
        self.assertEqual(model.coef_.shape, (len(STYLES), N_FEATURES + 1))
        self.assertEqual(model.intercept_.shape, (len(STYLES), ))
        self.assertEqual(set(model.classes_), set(STYLES))

    def test_train_with_unknown(self):
        dataset = get_default_training_dataset()
        dataset = clean_dataset(dataset)
        dataset = add_noise(dataset, random_state=0)
        dataset_unknown = generate_unknown(dataset, 5000, random_state=0)
        dataset = pd.concat([dataset, dataset_unknown])
        count_vectorizer, _, model = train(dataset, random_state=0)

        self.assertEqual(len(count_vectorizer.get_feature_names()), N_FEATURES)
        self.assertEqual(model.classes_.shape, (len(STYLES) + 1, ))
        self.assertEqual(model.coef_.shape, (len(STYLES) + 1, N_FEATURES + 1))
        self.assertEqual(model.intercept_.shape, (len(STYLES) + 1, ))
        self.assertEqual(set(model.classes_), set(STYLES + ['unknown']))


class EvaluationTestCase(TestCase):
    def test_evaluate_model(self):
        dataset = get_default_test_dataset()
        dataset = clean_dataset(dataset)
        dataset = add_noise(dataset, random_state=0)
        dataset_unknown = generate_unknown(dataset, 5000, random_state=0)
        dataset = pd.concat([dataset, dataset_unknown])

        model = get_default_model()

        accuracy, prediction = evaluate_model(dataset, *model)
        self.assertEqual(prediction.shape,
                         (dataset.shape[0], dataset.shape[1] + 1))
        self.assertEqual(prediction.columns.tolist(),
                         ['doi', 'string', 'style_true', 'style_pred'])
        self.assertEqual(set(prediction['style_pred']),
                         set(STYLES + ['unknown']))
        print(accuracy)
        self.assertTrue(accuracy > 0.94)

    def test_cv(self):
        dataset = get_default_training_dataset()
        dataset = clean_dataset(dataset)
        dataset = add_noise(dataset, random_state=0)
        dataset_unknown = generate_unknown(dataset, 5000, random_state=0)
        dataset = pd.concat([dataset, dataset_unknown])

        accuracies, prediction = evaluate_cv(dataset, get_default_algorithm(0))
        self.assertEqual(prediction.shape,
                         (dataset.shape[0], dataset.shape[1] + 1))
        self.assertEqual(prediction.columns.tolist(),
                         ['doi', 'string', 'style_true', 'style_pred'])
        self.assertEqual(set(prediction['style_pred']),
                         set(STYLES + ['unknown']))
        print(mean(accuracies))
        self.assertTrue(mean(accuracies) > 0.94)
